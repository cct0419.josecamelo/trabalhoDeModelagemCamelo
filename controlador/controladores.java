package controlador;

public class controladores {
	   private static Controlador controlador = null;
	    private Pessoa clienteCorrente = new Pessoa();
	    private ArrayList<Pessoa> pessoas = new ArrayList<Pessoa>();
	    private ArrayList<Produto> produtos = new ArrayList<Produto>();

	    static {
	        controlador = new Controlador();
	    }

	    public static Controlador getInstance() throws Exception {
	    	return controlador;
	    }
	    
	    private Controlador() {
	    	
	    }

	    public ArrayList<Produto> getProdutos() {
	        return produtos;
	    }

	    public void setProdutos(ArrayList<Produto> produtos) {
	        this.produtos = produtos;
	    }

	    public Pessoa getClienteCorrente() {
	        return clienteCorrente;
	    }
}
